/**
 * Slideshow Component
 * @class Slideshow
 *
 */
var Slideshow = (function(){

    var Constr,
        /**
         * Default Options
         */
        defaultOpts = {
            rootEl: "",
            slideSelector: '.slide',
            contentSelector: '.content',
            topicSlideSelector: '.topic',
            topicListId: '#topics',
            progressBarSelector: '#slideProgress',
            externalLinksSelector: '.ext',
            restartSelector: '.restart',
            modalSelector: '#modal',
            defaultAnimation: 'fade'
        };

    /**
     * Private Methods and Attributes
     */
    var currentSlide = 0,
        slideIds = [],
        /**
         * Build a list of Slides, by ID
         * @method buildSlideList
         */
        buildSlideList = function buildSlideList(){
            $(this.slideSelector).each(function(index){
                var id = $(this).attr('id');
                if(id === "" || id === undefined){
                    id = 'slide_' + index;
                    $(this).attr('id',id);
                }
                slideIds.push(id);
            });
        },
        /**
         * Escape html characters
         * @method htmlEscape
         * @params {String} str String to escape
         * @return {String} escaped string
         */
        htmlEscape = function htmlEscape(str) {
            return String(str)
                .replace(/&/g, '&amp;')
                .replace(/"/g, '&quot;')
                .replace(/'/g, '&#39;')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;');
        },
        /**
         * Move through slideshow by given amount
         * @method moveSlide
         * @param {int} direction movement vector
         */
        moveSlide = function moveSlide(direction){
            var nextSlide,
                thisSlide = slideIds[currentSlide],
                effect;
            if(currentSlide + direction < slideIds.length){
                nextSlide = slideIds[currentSlide + direction];
                effect = $("#" + nextSlide).data('transition-effect') ||
                    this.defaultAnimation;
                this.transition(thisSlide,nextSlide,effect);
            }
        };
    /**
     *
     * @param {Object} opts Configuration options
     * @constructor
     */
    Constr = function(opts){
        var options = $.extend(defaultOpts,opts);

        this.rootEl = $(options.rootEl);
        this.slideSelector = options.slideSelector;
        this.contentSelector = options.contentSelector;
        this.topicSlideSelector = options.topicSlideSelector;
        this.topicList = $(options.topicListId);
        this.progressBar = $(options.progressBarSelector);
        this.externalLinks = $(options.externalLinksSelector);
        this.restartLinks = $(options.restartSelector);
        this.modal = $(options.modalSelector);
        this.defaultAnimation = options.defaultAnimation;

        buildSlideList.call(this);

        this.setupTriggers();
        this.buildTopicList();
        this.styleCodeBlocks();
        this.hideModal();
        this.restart();
    };

    /**
     * Public methods and properties
     */
    Constr.prototype = {
        constructor: Slideshow,
        /**
         * bind click and keypress triggers
         *
         * @method setupTriggers
         */
        setupTriggers: function setupTriggers(){
            var that = this;

            //bind next to all mouse clicks on document
            $(document).click(function(){
                that.next();
            });

            //bind next to up/right, previous to down / left key presses
            $(document).keydown(function(e){
                switch(e.keyCode){
                    case 39:   //right
                        that.next();
                        break;
                    case 37:   //left
                        that.previous();
                        break;
                    case 38:  //up
                        that.previous();
                        break;
                    case 40: //down
                        that.next();
                        break;
                    default:
                        //nothing
                        break;
                }
            });

            //open external links in modal window
            this.externalLinks.click(function(e){
                e.preventDefault();
                e.stopPropagation();
                $.get($(this).attr('href'), function(data){
                    that.showModalWindow($("<pre class=javascript></pre>")
                        .html(htmlEscape(data)));
                    that.styleCodeBlocks();
                });
            });

            //handle restarting the slides
            this.restartLinks.click(function(e){
                e.stopPropagation();
                e.preventDefault();
                that.restart();
            });

            //clicking the modal window will hide it
            this.modal.click(function(){
                that.hideModal();
            });

            //resize modal to fill window on resize
            $(window).resize(function(){
               that.modal.css('height',$(this).height());
            });
        },

        /**
         * Build the list of Topics
         *
         * @method buildTopicList
         */
        buildTopicList: function buildTopicList(){
            var me = this;
            $(this.topicSlideSelector).each(function(){
                var topicTitle = $(this).find('h1').html(),
                    topicId = $(this).attr('id'),
                    node = $("<li></li>").addClass('actionable');

                node.html(topicTitle).click(function(e){
                        e.preventDefault();
                        e.stopPropagation();
                        me.goTo(topicId);
                    });
               me.topicList.append(node);
            });
        },
        /**
         * Apply Code-block Styling
         *
         * @method styleCodeBlocks
         */
        styleCodeBlocks: function styleCodeBlocks(){
          $('pre.javascript').snippet("javascript",{
              style: "acid",
              menu: false
          });

          $('pre.csharp').snippet("c#",{
              style: 'bright',
              menu: false
          });
        },

        /**
         * Transition from one slide to another
         *
         * @method transition
         * @param {String} fromSlide Slide ID
         * @param {String} toSlide Slide ID
         * @param {String} animation type of animation to use
         */
        transition: function transition(fromSlide,toSlide,animation){
            var nextSlide = fromSlide || slideIds[currentSlide];
            try{
                this.animations[animation](nextSlide ,toSlide);
            }
            catch(e){
                console.log(
                    new Error("Invalid Animation type: '" + animation + "'")
                );
                this.animations['none'](nextSlide ,toSlide);
            }
            currentSlide = slideIds.indexOf(toSlide);
            this.updateProgressBar();
        },

        /**
         * Animations:  available animation functions
         */
        animations: {
            fade: function fade(fromSlide,toSlide){
                $('#'+fromSlide).fadeOut(function(){
                    $('#'+toSlide).fadeIn();
                });
            },
            none: function none(fromSlide,toSlide){
                $("#" + fromSlide).hide();
                $("#" + toSlide).show();
            },
            blinds: function blinds(fromSlide,toSlide){
                $("#" + fromSlide).slideUp(function(){
                    $("#" + toSlide).slideDown();
                })
            },
            slideFade: function slideFade(fromSlide,toSlide){
                $("#" + fromSlide).effect('drop',function(){
                   $("#" + toSlide).fadeIn();
                });
            }
        },

        /**
         * Go To a specific slide
         * @method goTo
         * @param {String} toSlide Slide ID
         */
        goTo: function goTo(toSlide){
            this.transition(null, toSlide,'fade');
        },

        /**
         * Update the progress Bar
         * @method updateProgressBar
         */
        updateProgressBar: function updateProgressBar(){
            var curSlide = this.getCurrentSlideCount(),
                ttlSlides= this.getSlideCount(),
                progress = Math.round((curSlide / ttlSlides) * 100),
                bar = $("<span></span>");

            bar.html(curSlide + '/' + ttlSlides);

            this.progressBar.css('width',progress + '%');
            this.progressBar.html(bar);
        },

        /**
         * Show content in a Modal Window
         * @method showModalWindow
         * @param {String} content HTML content for modal window
         */
        showModalWindow: function showModalWindow(content){
            this.modal.html(content).show();
        },

        /**
         * Hide the modal window
         * @method hideModal
         */
        hideModal: function hideModal(){
            this.modal.hide();
        },

        /**
         * Get the current Slide's ID
         * @method getCurrentSlide
         * @return {String} ID of current slide
         */
        getCurrentSlide: function getCurrentSlide(){
            return slideIds[currentSlide];
        },

        /**
         * Get the current slide's position
         * @method getCurrentSlideCount
         * @return {Number} current Slide's position
         */
        getCurrentSlideCount: function getCurrentSlideCount(){
            return currentSlide;
        },

        /**
         * Get the slide count
         * @method getSlideCount
         * @return {Number} number of slides
         */
        getSlideCount: function getSlideCount(){
            return slideIds.length - 1;
        },

        /**
         * restart Slideshow
         * @method restart
         */
        restart: function restart(){
            $(this.slideSelector).hide();
            this.goTo(slideIds[0]);
        },

        /**
         * Next slide
         * @method next
         */
        next: function next(){
            moveSlide.call(this,+1);
        },

        /**
         * Previous slide
         * @method previous
         */
        previous: function previous(){
           moveSlide.call(this,-1);
        }
    };
    return Constr;
}());
